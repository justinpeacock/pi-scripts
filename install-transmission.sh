#!/usr/bin/env bash

# Update packages
apt-get update
apt-get upgrade -y
apt-get dist-upgrade

# Install Transmission
CHECK_TRANSMISSION=$(dpkg -s transmission-daemon)
if [[ $CHECK_TRANSMISSION != *"Status: install ok"* ]]; then
	apt-get install transmission-daemon -y
fi

usermod -a -G pi debian-transmission

# Enter Transmission Settings
echo ""
echo "Download directory:"
read DOWNLOAD_DIR
echo "Incomplete directory:"
read INCOMPLETE_DIR
echo "Transmission Username:"
read RPC_USERNAME
echo "Transmission Password:"
read RPC_PASSWORD

sed -i "s|\"download-dir\": \"/var/lib/transmission-daemon/downloads\"|\"download-dir\": \"${DOWNLOAD_DIR}\"|g" /etc/transmission-daemon/settings.json
sed -i "s|\"incomplete-dir\": \"/var/lib/transmission-daemon/Downloads\"|\"incomplete-dir\": \"${INCOMPLETE_DIR}\"|g" /etc/transmission-daemon/settings.json
sed -i "s|\"incomplete-dir-enabled\": false|\"incomplete-dir-enabled\": true|g" /etc/transmission-daemon/settings.json
sed -i "s|\"rpc-password\": \".*\"|\"rpc-password\": \"$RPC_PASSWORD\"|g" /etc/transmission-daemon/settings.json
sed -i "s|\"rpc-username\": \"transmission\"|\"rpc-username\": \"${RPC_USERNAME}\"|g" /etc/transmission-daemon/settings.json
sed -i "s|\"rpc-whitelist\": \"127.0.0.1\"|\"rpc-whitelist\": \"192.168.*.*\"|g" /etc/transmission-daemon/settings.json
sed -i "s|\"umask\": 18|\"umask\": 2|g" /etc/transmission-daemon/settings.json
sed -i "s|\"port-forwarding-enabled\": false|\"port-forwarding-enabled\": true|g" /etc/transmission-daemon/settings.json

# Reload Transmission
echo "Reloading transmission-daemon ang stopping...."
service transmission-daemon reload

# Stop Transmission
service transmission-daemon stop

# Reload daemon
echo "Reloading daemon..."
systemctl daemon-reload

# Start Transmission
echo "Starting Transmission..."
service transmission-daemon start


