# Pi Scripts

A few shell scripts for setting up my Raspberry Pi's.

## Helpful Links

* [pi-hole](https://pi-hole.net/)
* [Transmission](https://pimylifeup.com/raspberry-pi-torrentbox/)
* [LCD-show](https://github.com/goodtft/LCD-show) - LCD drivers for TFT screen
* [chronometer2](https://github.com/jpmck/chronometer2) - Stats screen for pi-hole
* [Resilio Sync](https://help.resilio.com/hc/en-us/articles/206178924-Installing-Sync-package-on-Linux)

## Install pi-hole

```bash
curl -sSL https://install.pi-hole.net | bash
```

## Install Samba

```bash
 sudo bash install-samba.sh
```

```bash
sudo nano /etc/samba/smb.conf
```

```
[passport]
   comment= Pi External HD
   path=/mnt/passport
   browseable=Yes
   writeable=Yes
   only guest=no
   create mask=0777
   directory mask=0777
   public=no
```