#!/usr/bin/env bash

## Update packages
apt-get update
apt-get upgrade -y
apt-get dist-upgrade

## Install Samba
CHECK_SAMBA=$(dpkg -s samba-common-bin)
if [[ $CHECK_SAMBA != *"Status: install ok"* ]]; then
	apt-get install samba samba-common-bin -y
fi

# Add the pi user and add password
smbpasswd -a pi