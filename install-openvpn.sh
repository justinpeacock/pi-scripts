#!/usr/bin/env bash

# This script will install OpenVPN and
# configure it for Private Internet Access VPN,
# including DNS leak prevention and kill switch.

# This script is intended for Raspberry Pi
# running Raspbian Stretch.

# This script needs to be run with sudo.
# ex. sudo ./install.sh

# Update packages
apt-get update
apt-get upgrade -y
apt-get dist-upgrade

# Install OpenVPN
CHECK_VPN=$(dpkg -s openvpn)
if [[ $CHECK_VPN != *"Status: install ok"* ]]; then
	apt-get install openvpn
fi

# Download PIA OpenVPN config files
mkdir /etc/openvpn/pia
wget -P /etc/openvpn/pia https://www.privateinternetaccess.com/openvpn/openvpn.zip
CHECK_UNZIP=$(dpkg -s unzip)
if [[ $CHECK_UNZIP != *"Status: install ok"* ]]; then
	apt-get install unzip
fi
unzip /etc/openvpn/pia/openvpn.zip -d /etc/openvpn/pia

cd /etc/openvpn/pia

# Copy the PIA OpenVPN certificates to the OpenVPN client
cp ca.rsa.2048.crt crl.rsa.2048.pem /etc/openvpn/

# Choose PIA server
echo ""
echo "Available servers:"
for f in *.ovpn
do
	echo "${f%%.*}"
done
echo ""
echo "Enter name of your desired PIA server from list above:"
read SERVER
cp "$SERVER.ovpn" /etc/openvpn/"$SERVER.conf"

# Enter PIA credentials and save to file "login.info"
echo ""
echo "Enter PIA login:"
read LOGIN
echo "Enter PIA password:"
read PASSWORD
cd /etc/openvpn
if ls login.info > /dev/null 2>&1; then
	rm login.info # if login.info exists, then delete it
fi
echo $LOGIN >> login.info
echo $PASSWORD >> login.info
chmod 400 login.info

# Add "login.info" to server config file
sed -i 's/auth-user-pass/auth-user-pass login.info/g' "$SERVER.conf"

# Enable VPN at boot
# @todo server name with spaces doesn't work
sudo systemctl enable openvpn@$SERVER

# Enable Port Forwarding
echo -e '\n#Enable IP Routing\nnet.ipv4.ip_forward = 1' | sudo tee -a /etc/sysctl.conf
sudo sysctl -p

# Setup NAT fron the local LAN down the VPN tunnel
iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
iptables -A FORWARD -i tun0 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i eth0 -o tun0 -j ACCEPT

# Make the NAT rules persistent across reboot
apt-get install iptables-persistent

# Make the rules apply at startup:
systemctl enable netfilter-persistent

# Block outbound traffic from the pi so that only the VPN and related services are allowed.
iptables -A OUTPUT -o tun0 -m comment --comment "vpn" -j ACCEPT
iptables -A OUTPUT -o eth0 -p icmp -m comment --comment "icmp" -j ACCEPT
iptables -A OUTPUT -d 192.168.1.0/24 -o eth0 -m comment --comment "lan" -j ACCEPT
iptables -A OUTPUT -o eth0 -p udp -m udp --dport 1198 -m comment --comment "openvpn" -j ACCEPT
iptables -A OUTPUT -o eth0 -p tcp -m tcp --sport 22 -m comment --comment "ssh" -j ACCEPT
iptables -A OUTPUT -o eth0 -p udp -m udp --dport 123 -m comment --comment "ntp" -j ACCEPT
iptables -A OUTPUT -o eth0 -p udp -m udp --dport 53 -m comment --comment "dns" -j ACCEPT
iptables -A OUTPUT -o eth0 -p tcp -m tcp --dport 53 -m comment --comment "dns" -j ACCEPT
iptables -A OUTPUT -o eth0 -j DROP

# Save so they apply at reboot
netfilter-persistent save